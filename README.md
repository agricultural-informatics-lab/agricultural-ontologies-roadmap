### Agricultural Ontologies Roadmap
This includes: identifying shared data ontologies, conversions between ontologies, finding overlap between existing data models. Data covered includes: ecological, plants, farm operations, etc.
 
* Current work: Conduct requirements elicitation interviews, assemble first cut ontology.
* [Issue Board:](https://gitlab.com/agricultural-informatics-lab/agricultural-ontologies-roadmap/-/boards) provides an overview of all currently planned activities and their status.
* Work Package Leads: @sudokita, @gbathree
* Resources: [Google Drive](https://drive.google.com/drive/folders/1kvXXEG7UcMJO-UTzwiENsLecGvUDf5C7?usp=sharing) meeting notes, interviews etc. [Airtable Base](https://airtable.com/invite/l?inviteId=invXncEhxdjSrFIoX&inviteToken=2e0b0c4016e5f161bffc3fa93f441a0e924b592de585b8376a652baef943d908) for the current state of the data and ontology itself.

**Deprecated resources**: The original discussion for openTEAM related ontology work can be [found here](https://gitlab.com/OpenTEAM1/OpenTEAM-Technology/-/issues/40). [Spreadsheet linked here](https://docs.google.com/spreadsheets/d/1f4QTadRhRARWY_gnlAzWGal00C99wH20Lm4BrSaA-mg/edit?usp=sharing) for posterity.

### Current Plan of Work (Fall)

- [x] Groundwork (get lists, specifications, use cases)
  - get lists from partners (Our Sci, Dagan, COMET, farmOS ... others)
  - add metadata to lists
- [ ] **NOW**: Requirements Elicitation Round 1: Plants Edition:
  - [ ] Conduct interviews to walk through plants + attributes ('must have' and 'would like') and other ag-related lists (tillage types, amendment lists, etc.). See [this issue](https://gitlab.com/agricultural-informatics-lab/agricultural-ontologies-roadmap/-/issues/2) for detail on the interviews.
  - [ ] Create set of use cases to be supported
  - [ ] Create set of functional requirements for Plant Data Service to meet openTEAM interoperability
  - [ ] Create first cut ontology for alpha testing by openTEAM members via airtable API.
- [ ] Development Round 1: Plants Edition: 
  - spec out exact details for dev / data manager along with hiring post.
- [ ] Requirements Eliciation Round 2: Tillage, Fertilizers, etc., as identified during round 1.

### Next Steps (Spring)
- [ ] TECHNICAL: Test out the shared ontology with integration between these tools: farmOS, landPKS, OurSci, CometFarm, Dagan, Regen).
- [ ] FARM/FIELD METHODS: Feedback from existing users of existing platforms 
- [ ] HCD: Implications of ontological decisions on usability and usefulness.

**What can we do if we have a supported developer / data manager (to be revisited post-interviews)**
- [ ] (now) Create / implement on shared lists information model for PDS storage
- [ ] (now) Put shared lists in new information model in Airtable dataset for future updating and Gitlab (or similar) for easy access
- [ ] (now) Review of existing collaborations, holders of ontologies, etc. which we can use (so we're not creating something new).
- [ ] (asap) The long-term sustainability of this storage! 
- [ ] (soon) (MongoDB) structure system for sharing (above) + available through gitlab
- [ ] connect pick-a-carrot dataset w/ PDS
- [ ] scrape amendments, pesticides, herbicides to update + improve lists
- [ ] integrate nutritional database into PDS
- [ ] (later) update to API (if/as needed)